\section{Design of NDN-Lite Pub/Sub}
\label{sec:design}
In this section, we show how NDN-Lite Pub/Sub provides a high-level API for NDN IoT with built-in security support.
Among numerous messaging patterns, the \textit{Publish-Subscribe (Pub/Sub)} pattern allows decoupling publishers and subscribers. 
Data is published by attaching it to \textit{resources} and notifying the system about the production of such data.
Intended recipients can subscribe to \textit{resources} by asking the system to deliver data of interest without actually knowing publisher addresses%
\footnote{In IP-based communication, the Pub/Sub systems we refer to here are often realized using a rendezvous node. However, as discussed later, a Pub/Sub system in NDN can be realized without such dependencies by semantically naming data.}.
% \philipp{Question: The system we refer to here is often realized using a rendezvous node in IP-based pub/sub. We don't use rendezvous nodes. Is this also one of our advantages?}
% \tianyuan{footnote for IP-based pub/sub and NDN-based pub/sub  (perhaps better to be in a discussion section)}
This property naturally fits into IoT applications' need for data-centric many-to-many communication.
Therefore, we chose publish-subscribe as the messaging pattern for our API.

\mypara{How to realize Pub/Sub in NDN}
%First, t
To realize Pub/Sub with NDN as the network layer, we rely on the application's use of semantic data naming.
Semantic information in Data names allows specifying \textit{resources} as name prefixes. Hence, subscribers can express the resources they are interested in by subscribing to a set of name prefixes.
%Second, the subscribers in the system need to know when the newly published Data becomes available.
%Third, 
Focusing on security, NDN-based security models (cf. Section~\ref{sec:background-security}) provide the foundation for automating security workflows and make them transparent for developers. Thus, an NDN-based Pub/Sub systems should embed name-based security models and guarantee that the received Data is verified and authenticated.
% Therefore, senders encrypt the payload and sign Data with trust policy allowed keys before publishing.
%The sender should encrypt the payload and sign Data with trust policy allowed keys before publishing.
%While the integrity of publications is ensured by cryptographic signatures, name-based rules -- referred to as security policies -- are used to define which participants are authorized to create and access which publications. 
% Technically, these policies allow authorized publishers and subscribers to access cryptographic keys for a publication based on its name. 
% This secures publications for a group of participants instead of for individual communication partners only.

\mypara{Overview of NDN-Lite Pub/Sub}
NDN-Lite organizes application data with a name tree (cf. Section~\ref{sec:nametree}).
Such a name tree describes the low-level application logic so that developers can focus on implementing higher-level application components.
Security policies are expressed by the homeowner in a controller application and specify trust relationships between name tree nodes (cf. Section~\ref{sec:policy}). %, and we demonstrate the policy enforcement can be automated leveraging names.
Home IoT functions are categorized into \textit{collecting sensor readings} and \textit{actuating commands}. NDN-Lite Pub/Sub provides a unified API for these function categories (cf. Section~\ref{sec:api}). 
The network-level realization of the API is described in Section~\ref{sec:realization}. 
In Section~\ref{sec:security}, we show NDN-Lite Pub/Sub's built-in security support. This security support is realized by automating security workflows for publishers and subscribers so that low-level cryptographic primitives are hidden from developers.

\subsection{Name Tree}
\label{sec:nametree}
\begin{table*}[t!]
	\newcolumntype{C}{>{\raggedright\let\newline\\\arraybackslash\hspace{0pt}}m{0.3\linewidth} }
	\newcolumntype{D}{>{\raggedright\arraybackslash} m{0.7\linewidth} }
	\resizebox{\textwidth}{!}{% <------ Don't forget this %
		\begin{tabular}{CD}
			\toprule
			\textbf{Type} & \textbf{Naming Convention} \\ \midrule
			Device\&Application Certificate
			& /<home-prefix>/<service>/<location>*/<participant-id>/KEY/<key-id> \\
			\midrule

			Device\&Application Policy
			& /<home-prefix>/<service>/<location>*/<participant-id>/POLICY/<timestamp>\\
			\midrule

			Application Data
			& /<home-prefix>/<service>/<location>*/<participant-id>/<data-id>/<timestamp> \\
			\midrule

			Encryption Symmetric Key
			& /<home-prefix>/<service>/ACCESS/EKEY/<key-id>/<timestamp>\\

			Decryption Symmetric Key
			& /<home-prefix>/<service>/ACCESS/DKEY/<key-id>/<timestamp>\\
			\bottomrule
		\end{tabular}% <------ Don't forget this %
	}

	{
		\footnotesize
		Notation: A component with <> represent a variable, e.g., /<> can be /TEMP representing temperature service.
		A component with <>* represent a variable that may consists of multiple name components,  e.g., /<location>* can be /kitchen or /kitchen/sensor-123.
	}
	\caption{Data Naming Conventions}
	\label{tab:naming_convention}
	\vspace{-0.7cm}
\end{table*}

In NDN-Lite Pub/Sub, we group smart home data based on two high-level properties: \textit{service} and \textit{location}.
The \textit{service} describes the abstract home service to which the Data is related, while the \textit{location} specifies the physical scope the Data is associated with.

\begin{figure}[t]
	\centering
	\includegraphics[width=0.47\textwidth]{figures/name-tree}
	\vspace{-0.2cm}
	\caption{Sketch of an example name tree}
	\label{fig:nametree}
	\vspace{-0.2cm}
\end{figure}

With the above understanding, we design our name tree with a functionality-driven principle in mind.
Table~\ref{tab:naming_convention} summarizes the structure of names and the overall namespace used by NDN-Lite Pub/Sub, and Figure~\ref{fig:nametree} shows a sketch of an example name tree based on the afore-mentioned naming conventions.

The overall namespace in the example from Figure~\ref{fig:nametree} starts with \textit{alice-home} as the system's root prefix.
% \tianyuan{use alice-home for consistency (currently it looks like starting a new story}
% This name component can be \textit{alice-home} to reflect real-world ownerships.
The name tree has the branches \textit{AUTOMATION} and \textit{LOCK} to represent two example services of the smart home\footnote{One can have more branches if having more services (e.g., temperature, motion) in the system.}.
The \textit{AUTOMATION}-service represents the control module of the smart home and aggregates all home automation application data (eg., applications that trigger a fire alarm when smoke is detected).
Data of door locks is aggregated by the \text{LOCK}-service.
Under the \textit{LOCK} name tree node, the name components \textit{livingroom} and \textit{front} represent locations%
\footnote{A location consists of zero to two name components (e.g., \name{/}, \name{/livingroom}, \name{/livingroom/front}), depending on the required granularity.}.
A tuple \textit{<service, location>} can be used to specify a name tree node with which publishers and subscribers may interact.
For example, Data containing self-state reports produced by the living room's front door lock can be considered under the name tree node specified by the tuple \textit{<LOCK, /livingroom/front>} (cf. dashed red arrow).

Under the door lock's location, \textit{lock-1} node represents the door lock's participant identifier.
Certificates and security policies for it are held under corresponding subtrees.
Meanwhile, subtrees that start with \textit{state} and \textit{set-lock} further differentiate participant's Data based on application semantics (cf. highlighted by the dashed green circle).
The periodic self-state report of door lock is published with \textit{state} as data-id. A command to control the door (eg. lock) uses \textit{set-lock} as data-id.
A \textit{timestamp} is attached as the last name component to achieve uniqueness among all application data names.

The Data content produced by the door lock is encrypted by symmetric encryption keys. These keys are provided by the controller, and authorized subscribers are capable to decrypt Data with decryption keys obtained from the controller.
Such encryption and decryption keys are held under the \textit{EKEY} and \textit{DKEY} subtree in the \textit{ACCESS} branch of the \textit{LOCK} service (highlighted by the dashed blue circle).

\subsection{Security Policies}
\label{sec:policy}
Security policies (Section~\ref{sec:background-security}) enable authorization, in addition to access control within the name tree structure.
Therefore, participants are limited to create or access Data only under certain name tree nodes, specified by the policies.
A security policy expresses the relationship between name tree nodes and key names, where key names can again be seen as nodes in the name tree. 
Formally, a security policy is defined as a triple \textit{<DataNamePrefix, PolicyType, KeyNamePrefix>}, where the \textit{DataNamePrefix} defines the targeted node in the name tree. 
The \textit{PolicyType} can either allow \textit{signing} data matching the targeted node, or allow \textit{access} to a service defined by the node. 
The \textit{KeyNamePrefix} defines the prefix of key names for which the policy applies.
While the definition of security policies is left to the application layer\footnote{Eg. policy definition via the GUI of a controller, as envisioned by NDN-Lite.}, NDN-Lite Pub/Sub supports integrity and authenticity protection based on security policy definitions.
Each participant fetches its security policies, which include both signing and access policies from the bootstrapping step.
Security policies must be signed by the controller thus verifiable with participants' installed trust anchor by looking into the signing certificate name and signature value.

\begin{figure}[t]
	\centering
	\includegraphics[width=0.47\textwidth]{figures/name-tree-policy}
	\vspace{-0.2cm}
	\caption{Security policies over name tree nodes}
	\label{fig:nametree-policy}
	\vspace{-0.2cm}
\end{figure}

The name tree in Figure~\ref{fig:nametree-policy} provides two cases as explained below to show how our Pub/Sub automatically enforces security policies leveraging names.
Later, we show the realization of automated security workflow in Section~\ref{sec:security}.

\mypara{Automatic signing policy enforcement}
The security policy \ding{202}:

\textit{</alice-home/LOCK, sign, /alice-home/LOCK>}

restricts the \textit{LOCK}-service Data to be signed only by identity keys under the \textit{LOCK}-service prefix.
The signing relationships of published Data can be automatically verified by examining the each step along the certificate chain.
Later when receiving newly published Data, subscribers can extract signers' certificate name by looking the KeyLocator field of Data, then check against fetched signing type policies to determine if the signing key is authenticated to publish with received Data name.
If the signing key is authenticated, subscribers fetch the signer's certificate, and the retrieved certificate is examined with KeyLocator field to see if signed by the installed trust anchor, and later the public key of installed trust anchor is used to validate the signer's certificate.
After, public keys contained in the validated signer's certificate is used to verify the signature of the published Data.

\mypara{Automatic access policy enforcement}
The policy \ding{203}:

\textit{</alice-home/LOCK, access,}

\hspace{2cm} \textit{/alice-home/AUTOMATION/hub/app-1>}

allows all participants holding certificates under prefix \name{/alice-home/AUTOMATION/hub/app-1} to access Data of the \textit{LOCK}-service.
Having learned the permission to access from the policy definition, \name{app-1} fetches its decryption key to get access.
Guided by access policies and the identity information carried in the decryption key request, controller chooses either accepting that request by returning the decryption key or denying by returning the error code.
The decryption key follows the naming convention in Table~\ref{tab:naming_convention}.
After getting the Data, subscribers can extract the service name \textit{LOCK} from the published Data's name components, and use corresponding decryption key to access Data content.

\subsection{Providing API for Different Applications}
\label{sec:api}
After designing the name tree where all application data are organized, we now consider developing easy-to-use API to enable developers interacting with the name tree.
Our API design starts with two observations on home IoT applications and ends up with unified API that can serve diverse applications.

A key observation is that IoT applications have two fundamental publishing behaviors when adopting a publish-subscribe design pattern: publish periodically, or publish at random times.
Data publishing at random times are triggered events (e.g., fire alarms and commands to turn the camera off/on).
The publishing of such data is primarily triggered by sudden changes in the physical environment and users' input.
In contrast, periodically published data are periodic events.
For example, a temperature sensor might be scheduled to report the sensing data on a minute basis.
NDN-Lite Pub/Sub provides a unified API for both types of publishing behaviors by letting publishers decide their publishing modes.

A second observation is applications have various real-time requirements.
For instance, the primary goal of temperature monitoring application is to collect sensor readings from various rooms, which has a relatively low real-time requirement.
In contrast, the smoke detection application, which needs to promptly react to abnormal events (e.g., raise alarms as soon as possible when smoke detected), has a relatively high real-time requirement. %\philipp{Minor: Is triggering the alarm high real-time? If this is done within a couple of minutes, it is probably fine. Whats with closing the window if it starts raining? Or starting the sprinkler on fire?}
Thus API design should respond to this difference in real-time requirements with easy-to-use interfaces.

Based on these two observations, along with the concept of name tree, NDN-Lite Pub/Sub provides API as follows:
\begin{figure}[ht]
	\centering
	\includegraphics[width=0.5\textwidth]{figures/api}
	\vspace{-0.2cm}
	\label{fig:api}
	\vspace{-0.5cm}
\end{figure}

\mypara{Publish API}
The Publish API allows publishers to specify a name tree node by using \textit{<service, location>} tuple, and attach Data with specified names (derived from the input parameters and naming convention) and \textit{content}.
It requires publishers selecting their publishing modes (either periodic or random) by \textit{pattern} when calling.

\mypara{Subscribe API}
The Subscribe API allows subscribers to specify a name tree node, and expressing Interest with \textit{callback} and set the real-time requirement by \textit{interval}.
For example, a \textit{3\_s} interval means the subscriber expects to receive data no later than 3s after its publishing time.
% The callback can be only be triggered once within this interval. \philipp{I see the technical reason for this, but is it good to being able to trigger only once during the interval? What happens if two events occur during the iterval? Are there use-cases for that?}

\subsection{Protocol Design of the Pub/Sub API}
\label{sec:realization}
%\tianyuan{1. interest retransmission rate, not dissemination delay
%          2. do all subscribers use the same interest retransmission timer value?
%          3. if we have "long pull" already, what's the point of NOTIFY?}


\begin{table*}[t!]
	\newcolumntype{C}{>{\raggedright\let\newline\\\arraybackslash\hspace{0pt}}m{0.3\linewidth} }
	\newcolumntype{D}{>{\raggedright\arraybackslash} m{0.7\linewidth} }
	\resizebox{\textwidth}{!}{% <------ Don't forget this %
		\begin{tabular}{CD}
			\toprule
			\textbf{Type} & \textbf{Naming Convention} \\ \midrule
			Published Data 
			& /<home-prefix>/<service>/<location>*/<participant-id>/<data-id>/<timestamp> \\
			\midrule
			
			Prefix Registered for Published Data
			& /<home-prefix>/<service>/<location>*/<participant-id>/<data-id>/<timestamp> \\
			\midrule
			
			Periodic Interest
			& /<home-prefix>/<service>/<location>* \\
			\midrule
			
			Notification Interest
			& /<home-prefix>/<service>/<location>*/NOTIFY/<published-data-name>* \\
			\midrule
			\bottomrule
		\end{tabular}% <------ Don't forget this %
	}
	
	{
		\footnotesize
		Notation: A component with <> represent a variable, e.g., /<> can be /TEMP representing temperature service.
		A component with <>* represent a variable that may consists of multiple name components,  e.g., /<location>* can be /kitchen or /kitchen/sensor-123.
	}
	\caption{Protocol Naming Conventions}
	\label{tab:protocol_naming}
	\vspace{-0.7cm}
\end{table*}


While the previous section described our API's design, this section presents the proposed protocol used to realize Pub/Sub with Interest-Data exchange.
Figure~\ref{fig:realization} visualizes the workflow from the application perspective, including the tasks necessary in the lower layers of the protocol stack.
We summarizes the naming convetion of proctocols in Table~\ref{tab:protocol_naming}.

%In this section, we describe how we realized our Pub/Sub APIs with Interest-Data exchanges.

\begin{figure}[ht]
	\centering
	\includegraphics[width=0.8\linewidth]{figures/realization}
	\vspace{-0.2cm}
	\caption{Application-to-application workflow showing all layers involved in the Pub/Sub process.}
	\label{fig:realization}
	\vspace{-0.2cm}
\end{figure}

We begin by describing the protocol on the publisher-side of the system. The publisher-side can choose among random and periodic publishing, with the protocol design differing only slightly.
When a publisher application publishes new Data using the \textit{publish API}, the \textit{service} and \textit{location} parameters define the Data's name prefix. The \textit{data-id} is used as an additional name component to identify the new publication with application semantics. The API fills \textit{participant-id} and \textit{timestamp} into names, creates and buffers the Data locally, and registers the Data's exact name at the forwarder.

In \textit{random publishing} mode (e.g., used for sending control commands to devices), the publisher generates a Notification Interest to notify all subscribers about the new publication. 
The Notification Interest's name starts with the prefix defined by the \textit{service} and \textit{location} parameters and adds a \texttt{/NOTIFY}-suffix, followed by the exact name of the published Data. 
This Interest name allows subscribers to fetch the randomly published Data on receipt of the Notification Interest. 
To preclude packet loss, Notification Interests are repeated until acknowledged by at least one subscriber\footnote{A pre-set maximum number of repetitions prevents infinite notifications in the case of zero subscribers. When no acknowledgment is received, a failure is reported to the calling application.}.

In \textit{periodic publishing} mode (e.g., used when producing sensor data), the Notification Interest is omitted. 
In this case, periodically sent Subscribe Interests from subscribers (explained in the following) are used to retrieve published Data.

When focusing on the subscriber-side of the system, subscriber applications express interest to Data under a name tree node by using the \textit{service} and \textit{location} parameters of the \textit{subscribe API}.
Besides, the application provides a callback method that defines the application's action on receiving a publication. 
The \textit{interval} parameter defines the frequency the applications want to receive updates. 
Calling the subscribe API triggers two actions: First, to receive Data, the subscriber emits periodic \textit{Subscribe Interests} holding the \textit{service} and \textit{location} parameters as name prefix. 
The configured interval defines the frequency of those Subscribe Interests\footnote{Different subscriber applications may have different intervals set}. %This ensures staying within the desired dissemination delay. 
Whenever published Data matches the \textit{service} and \textit{location} parameters, the Subscribe Interests initiate delivery of thereof.
Second, the \textit{subscribe API} configures the forwarder to listen to Notification Interests matching the \textit{service} and \textit{location} parameters. 
Therefore, the name prefix holding the \textit{service} and \textit{location} parameters is registered with the suffix \textit{/NOTIFY} appended. 
This enables the Notification Interests sent by publishers in random publishing mode be forwarded to the subscriber applications.

Note that subscribers do not need to know the publisher's publishing behavior ahead of time.
A subscriber may receive a Notification Interest that enables the subscriber to immediately fetch randomly published Data, and additionally, actively pulls periodic publications by emitting Subscribe Interests. Active pulling, however, might not be reasonable in all application use cases (eg. pulling infrequent control commands) and, hence, can be disabled, eg. by setting the \textit{interval} parameter to zero. In this case, Data transmission is triggered by Notification Interests only.

In NDN-Lite Pub/Sub, consumer's knowledge of data publication is either obtained by its periodic pulling, or listening on broadcast Notification Interests.
The above two mechansims form a simple transport function similar to NDN Sync~\cite{li2018brief}, although with home IoT focused applications and protocol design.

\subsection{Security Workflow}
\label{sec:security}
The NDN-Lite Pub/Sub API provides built-in security support for applications by automating the security workflows required for Data publishing and subscribing.
%
%By embedding all security primitives in a Data's lifecycle are handled by NDN-Lite Pub/Sub.
%
In this section, we demonstrate the NDN-Lite Pub/Sub's security workflow by an example (cf. Figure~\ref{fig:pubsub-example}), where an application (\name{/alice-home/AUTOMATION/hub/app-1}) running on a home controller subscribes to the self-state reports of the \textit{LOCK}-service at the living room's front door.
We assume that the security policies \ding{202} and \ding{203} from Figure~\ref{fig:nametree-policy} were retrieved by the application during the bootstrapping phase.

\begin{figure}[t]
	\centering
	\includegraphics[width=0.47\textwidth]{figures/pubsub-example}
	\caption{Subscribe to periodically published Data}
	\label{fig:pubsub-example}
	\vspace{-0.5cm}
\end{figure}

The participant \textit{lock-1} periodically produces the self-state report by calling the publish API with \textit{state} as \textit{data-id} and the current status (eg. open, or closed) as value.
The Pub/Sub module constructs the Data name following naming conventions, retrieve the encryption key, sign the Data packet with the private key of its identity, and make it available on the network.
% (see the components relations part in Sec~\ref{sec:implementation})
%, sign the Data packet with the private key for identity \name{/alice-home/LOCK/livingroom/front/lock-1}, then make it available to the network.

The \textit{app-1} subscribes to Data associated with \textit{LOCK}-service and \textit{$/$livingroom$/$front} location with a callback and interval being 3\_s. %\textit{3$_$s}.
Upon receiving new Data, \textit{app-1}'s Pub/Sub module checks security policy \ding{202} to examine if allowed keys sign the Data.
Its Pub/Sub module will then verify the signature carried in received Data against the fetched signer's (i.e., \textit{lock-1}) certificate.
Afterwards, the Data content is decrypted by Pub/Sub using corresponding decryption key since the security policy \ding{203} indicating \textit{app-1} has permission to access. %(see Sec~\ref{sec:implementation})%.
Pub/Sub module will only deliver received Data to the application if it passes all security checks.
In this example, data-id \textit{state} and content \textit{closed} is the final result delivered to the callback.

Note that the use of application names for data and keys enables NDN-Lite Pub/Sub, and the security workflow is automated based on the reasoning of relations between names.