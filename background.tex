\section{Background}
\label{sec:background}
\subsection{Named-Data Networking}
\subsubsection{Naming}
Named Data Networking (NDN)~\cite{zhang2014named} is a data-centric Internet architecture, which directly uses application-layer names for networking; there is no address in an NDN network.
Each data piece is identified by the name given by the application, and application naming generally follows a set of established rules -- noted as naming convention~\cite{yu2014ndn} -- agreed between data producer and consumer applications.
%% (e.g., naming convention name conventions indicating versioning and timestamp, as shown later in Section~\ref{sec:background-security} and shown in Table~\ref{tab:naming_convention})
Naming data enables one to secure data directly: a producer can uses its own key to cryptographically sign every data packet it produces, binding the packet's name to the content. 
Therefore, an Data packet consists of Name, Content, and Signature as shown in Figure~\ref{fig:packet}, making the packet verifiable by any consumer, independent of the underlying communication channels.

\begin{figure}[t]
	\centering
	\includegraphics[width=0.47\textwidth]{figures/packet}
	\vspace{-0.2cm}
	\caption{Example for Interest and Data packets}
	\label{fig:packet}
	\vspace{-0.5cm}
\end{figure}

\subsubsection{Interest-Data Exchange}
In an NDN network, a consumer can fetch Data by sending an Interest packet.
Interests contain either the exact data Name or a Name prefix of the requested Data. Once emitted, an Interest is forwarded through the network based on its name on a hop-by-hop base until it reaches a node holding a copy of the requested Data. The Data travels back on the reverse path of the Interest, realized by the Interest leaving breadcrumbs on each forwarding node, implemented as an entry in the so-called Pending Interest Table (PIT). To preserve flow-balance, one Interest can retrieve at most one Data packet. The example provided in Figure~\ref{fig:packet} shows the packet structure for Interest and Data packets.
Reliability in data delivery is consumer-driven in NDN. The consumer defines a retransmission time for each outstanding Interest; if an Interest's lifetime expires without receiving Data, the consumer may choose to re-express the Interest.


\subsubsection{Security}
\label{sec:background-security}
%%NDN aims to secure data directly and achieves data authenticity and confidentiality independent of the underlying communication channels.
%To achieve these security goals and provide usable security for applications, NDN uses public-key cryptography as a tool. 
NDN's security design contains four key components to achieve secure communication. 
They are trust anchors, identities, certificates, and security policies.
The trust anchor needs to be configured into all participants in an NDN network in a secure  out-of-band manner.
Each participant (which can be a device, an app, or a user) has a name and a certificate issued by the trust anchor; we refer to them as \textit{entities} in the rest of this section~\cite{zhang2018overview}.
Certificates and security policies also possess semantically meaningful names. Thus they are named, secured data, and can be retrieved via Interest-Data exchange. 
This allows automating security workflows, as done in our Pub/Sub security workflow, as elaborated in Section~\ref{sec:policy}.
%In the rest of this section, we use \textit{entities} to represent applications and all other communication participants in an NDN network~\cite{zhang2018overview}.

%They are all semantically named and retrievable via Interest-Data exchanges, thus enabling us to automate our Pub/Sub's security workflow in Sec~\ref{sec:policy}.
Figure~\ref{fig:security-overview} visualizes the relation of the four security components in an example application. In this example, a smart homeowner Alice owns both a temperature sensor, which produces temperature data, and an air-conditioner.
The latter is responsible for controlling the room temperature, therefore it needs to consume the sensor's data.
\begin{figure}[ht]
	\centering
	\includegraphics[width=0.47\textwidth]{figures/security-overview}
	\vspace{-0.2cm}
	\caption{Relationship between /alice-home, /alice-home/temp/sensor-123, and the data produced by the sensor.}
	\label{fig:security-overview}
	\vspace{-0.2cm}
\end{figure}
% \lixia{sensor ID is 123, key ID better uses a diff number}
\begin{itemize}
	\item \textit{Trust Anchors:}
	A trust anchor is the combination of the name of a trusted entity and its pair of trusted asymmetric keys~\cite{cooper2005rfc}.
	A trust anchor certificate is a self-signed certificate and installed on all entities via an out-of-band channel. 
	This certificate is the root of trust for all the entities in the system under the trust anchor, and is used to validate certification chains in public-key certificates.
	%All cryptographic verifications must terminate at a pre-established trust anchor.
	%The trust anchor is usually \tianyuan{“usually” needs a footnote} a self-signed certificate installed securely via an out-of-band mechanism.
	With the trust anchor's certificate installed, an NDN entity can verify Data signatures produced by other entities by validating their certificates along the certification chain.
	In the example application, Alice may generate a self-signed certificate named \name{/alice-home/KEY/222/self/001} as the trust anchor certificate. 
	This certificate is installed on the temperature sensor and the air-conditioner eg. by scanning a QR-code. 
	This allows the verification of certificates issued by Alice and ultimately, to verify Data produced by each other.

	\item \textit{Identities and Certificates:} In NDN, every entity that produces Data can be identified by its identity. 
	An identity is realized as a public-private key pair bound to a name, where the public key is embedded in named certificates. 
	The system's trust anchor certifies identities by binding an entity's public-key to a certificate name, under which it becomes available for other entities.
%    An NDN certificate issuer represents the certification to an identity by binding the its certificate name and public key.
	Certificate names match the following naming convention \name{/<prefix>/KEY/<key-id>/<issuer-info>/<version>}.
	In the example in Figure~\ref{fig:security-overview}, the temperature sensor's certificate is issued by the trust anchor. 
	The certificate name conveys the temperature sensor as the certificate owner and a key ID (ie. \textit{223}) identifying the public-private key pair belonging to the certificate. 
	Besides, it indicates that the trust anchor is the signer of the certificate and includes \textit{001} as the certificate's version number.

    \item \textit{Security Policies:} In order to authenticate Data and enable access control, trust rules~\cite{yu2015schematizing} and named-based access control~\cite{zhang2018nac} policies are also needed. 
    Accordingly, applications can define security policies to
	\textcircled{1} restrict Data under specific namespace only allowed be \textit{signed} by specific keys.
    \textcircled{2} restrict Data under specific namespace only allowed be \textit{accessed} by specific identities.
	Assuming Alice only wants to authenticate temperature sensors to produce temperature data, and allow the air-conditioner to access the temperature data.
	Therefore, Alice specifies a signing policy (the red box in Figure~\ref{fig:security-overview}) that Data under prefix \name{/alice-home/temp} can only be signed by keys under the same prefix.
    Meanwhile, an access policy (the green box in Figure~\ref{fig:security-overview}) is also defined to restrict Data under \name{/alice-home/temp} can only be accessed by identities under prefix \name{/alice-home/aircon}, limiting them only accessible by \name{aircon-456}.
    Then Alice can use the trust anchor to sign two Data containing the above two policies respectively, and let the the temperature sensor and air-conditioner to fetch and install their policies.
\end{itemize}
The security policies, together with trust anchor, identities and certificates enable NDN to achieve the data authenticity goal.
These four components have to be installed into an NDN entity in its bootstrapping step.
To achieve the data confidentiality goal and realize access control based on security policies, NDN employs encryption keys to protect the data content.

\subsection{NDN-Lite}
%% NDN-Lite aims to bring the control of smart homes from the clouds back to users' hands.
NDN-Lite is a lightweight version of the NDN implementation that fits into resource-constrained devices, commonly used in smart home environments. Its design aims to enable a secure, local user-controlled home IoT system.
NDN-Lite unifies home devices and applications as \textit{participants}. When using NDN-Lite, the homeowner sets up a controller as the local trust anchor, which issues certificates for participants and can control the whole system on behalf of the home users.
Besides, NDN-Lite implements high-level application support, such as security bootstrapping, access control, trust management.
%An overview of NDN-Lite's architecture is shown in Figure~\ref{fig:ndn-lite}.
%, and an NDN-Lite controller is set up by user to issue certificates and keys for participants.
%The controller serves as a local trust anchor thus can control the whole system on behalf of the home users (e.g., issue commands).
%
NDN-Lite's design makes the following assumptions.
\begin{itemize}
	\item Broadcast Network: All system participants are connected to a broadcast network (i.e., single-hop home wireless network).
	\item Device capabilities: All home devices are capable of being wireless connected, performing asymmetric key signing (e.g., ECDSA), and keeping adequate memory to store information for NDN-Lite.
	\item Security: The controller is trustworthy, and all system participants can be securely bootstrapped.% but can be compromised afterward.
\end{itemize}

%\mypara{Bootstrapping}
New smart home participants are bootstrapped by the homeowner through the controller using bootstrapping protocols, such as SSP~\cite{ssp-li} or NDNViber~\cite{ramani2020ndnviber}.
As indicated in Section~\ref{sec:background-security}, participants obtain the trust anchor, the identity, and security policies from this bootstrapping step. 
% in order to enable the system works properly.
% Using her smart phone as the controller, Alice can the QR code printed on the Figure~\ref{sec:background-security}'s temperature sensor and install the necessary materials mentioned above into the sensor.
% Then the temperature sensor needs to fetch the security policies \name{/alice-home/temp/policy/<timestamp>} to learn the policies for name prefix \name{/alice-home/temp}.

%\mypara{Access Control}
To restrict the participants' access to Data under permitted name prefixes only, NDN-Lite uses named-based access control~\cite{zhang2018nac} and symmetric encryption. After the bootstrapping process, participants request encryption and decryption keys for specific name prefixes from the controller. After verifying that the security policies allow access, the controller grants access by securely delivering the corresponding symmetric keys to the participant.